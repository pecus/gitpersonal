Introducing `gitpersonal`, `gitwork` and `gitdefault`, a set of utilities to manage multiple git profiles.

## Who is it for

Anyone who needs to manage git projects with different SSH users/git accounts, typically when using a work account and a personal account on gitlab, github or your favourite git provider.

## How it works

It automates switching git profiles, by creating and applying custom git configurations (per project or when cloning).

## Pre-requisites

`gitpersonal` has been tested only on Mac OS X, but should work on most Unix machines.

You should have created SSH keys for each git account. If you need help, you can follow instructions from [github](https://help.github.com/en/github/authenticating-to-github/connecting-to-github-with-ssh).

## How to install

Currently `gitpersonal` is available as a standalone download, and requires manual installation.

1. [Download](./gitpersonal) `gitpersonal` and save it in your PATH (either in `/usr/local/bin`, `~/bin` or anywhere else, possibly adding the directory to your shell PATH. If you are curious to what is in your PATH, run `echo $PATH`).
1. `chmod` it so that it is executable (`chmod u+x gitpersonal`).
1. make aliases or symbolic links for the three variants (`ln -s ./gitpersonal ./gitwork`, `ln -s ./gitpersonal ./gitdefault`).

## How to use

`gitpersonal [OPTIONS] [init | COMMAND]`

OPTIONS:
-h, --help      display this help message
-v, --version   version info

`gitpersonal`, `gitwork`, and `gitdefault`, are commands that change the behaviour of git.  
Specifically, they use a predefined SSH profile to easily allow switching between personal and work profiles.

The command should be invoked **within a git repository folder**, to configure it to use either the default, a work or personal profile. It can also be **invoked as a prefix to a remote git operation** (e.g. `git push`, `git pull`, `git fetch`, `git clone`, ...) to perform it with the corresponding user profile, i.e. `gitpersonal git clone git@github.com/user/repo`.

To use `gitpersonal` or `gitwork` you need a config file. These are invisible files named `.gitconfig_gitpersonal` and `.gitconfig_gitwork`, and live in your home directory. You don't need to create both, since you already have a default profile configured by git, but you need at least one.

If your default profile is for work, you will want to create a `.gitconfig_gitpersonal` config file. You can then use `gitpersonal` to clone or update projects to use your personal profile. To create a profile, use the `init` command, i.e. `gitpersonal init`, and answer the prompts. You will need to enter:

* the name you will associate with commits for this profile
* the email address you will associate with commits for this profile
* the name of the ssh key in your `.ssh` home directory for this profile

You can also accept the default, but you will need to edit the config file created by the utility to update it with real data.

```TOML
# multi user git config start
[core]
        sshCommand="ssh -i ~/.ssh/your_personal_ssh_key"
[user]
        name=Your Name
        email=your@email.address
# multi user git config end
```

When launching `gitpersonal` in a git repository folder, your personal configuration will override the default.
You can rely on `gitdefault` to reverse any changes done, inheriting the global configuration.

You can also use `gitpersonal` and its aliases as a prefix to a git remote command, such as `git clone`. This will perform the operation with the SSH user profile selected:
```bash
gitpersonal git clone git@github.com/user/repo.git
```

## Download
[gitpersonal](./gitpersonal)

## License
This software is released under MIT license